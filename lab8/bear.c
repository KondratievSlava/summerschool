//#include <stdio.h>
//#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include "myqueue.h"

#define COUNTBEE 2
#define BEEWORK 2
#define BEAREAT 4
#define STARTHONEY 10

int bee(int pidP)
{
    printf("hello, it's bee from %d\n",pidP);
    return 1;
}

int main( int argc, char *argv[])
{
    char s[2][10];
    char s1[10], s2[10];
    int honeyBee, honey = STARTHONEY, i, msgqid;; // stat, status;
    pid_t pid[COUNTBEE];
    struct mymsgbuf qbuf;
    struct msqid_ds info;
    key_t key;
    long qtype = sizeof(struct mymsgbuf) - sizeof(int);

    //int pidPar = getpid();
    key = ftok(".", 'm');
    if((msgqid = msgget(key, IPC_CREAT|0660)) == -1)
    {
        printf("can't create queue\n");
        //perror("msgget");
        exit(1);
    }
    msgctl(msgqid, IPC_STAT, &info);
    printf("mes in qu = %d\n", (int)info.msg_qnum);

    sprintf(s[1], "%d", BEEWORK);
    sprintf(s[0], "%d", msgqid);
    sprintf(s1, "%d", msgqid);

    sprintf(s2, "%d", BEEWORK);
    //printf("%d\n%s\n",pid,s);
    printf("msgqid=%s\n",s[0]);
    for (i = 0; i < COUNTBEE; i++)
    {
        pid[i] = fork();
        if (pid[i] == -1)
        {
            printf("error fork");
            exit(-1);
        } else if (pid[i] == 0)
        {
            //if(execl("aaa", "2", "2", "2", NULL) < 0);
            //if(execv("bee", s) < 0)
            if(execl("bee", s1, s2, NULL) < 0)
            {
                printf("error bee\n");
                exit(-2);
            }
            printf("bee %d no started\n", i + 1);
            exit(3);
        }
    }
    i = 5;
    printf("was %d honey\n", honey);
    //
    while ( i != 0 && honey >= BEAREAT) //жизнь медведя
    {
        sleep(BEAREAT); //ест
        honey -= BEAREAT;
        printf("honey after eating %d\n",honey);
        sleep(BEAREAT); //спит

        //тырим мед у пчел
        msgctl(msgqid, IPC_STAT, &info);
        printf("mes in qu = %d\n", (int)info.msg_qnum);
        if ((int)info.msg_qnum > 0)
        {
            while ((int)info.msg_qnum > 0)
            {
                honeyBee = read_message(msgqid, &qbuf, qtype);
                honey += honeyBee;
                msgctl(msgqid, IPC_STAT, &info);
            }
            printf("honey was apt to %d\n", honey);
        }
    }
    for (i = 0; i < COUNTBEE; i++)
    {
        kill(pid[i], SIGKILL);
        printf("kill %d proc\n", i + 1);
    }

    if ((i = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
        printf("msgctl (return queue) failed, rc=%d\n", i);
        return 1;
    }

    return 0;
}
