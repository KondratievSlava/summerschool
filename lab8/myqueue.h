#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <unistd.h>

struct mymsgbuf
{
    long mtype;
    int honeyBee;
};

int read_message(int qid, struct mymsgbuf *qbuf, long type)
{
    qbuf->mtype = type;
    msgrcv(qid, (struct msgbuf *)qbuf, sizeof(int), type, 0);
    printf("Type: %ld honey = %d\n", qbuf->mtype, qbuf->honeyBee);
    return qbuf->honeyBee;
}

void send_message(int qid, struct mymsgbuf *qbuf, long type, int honey)
{
    printf("put in qu\n");
    qbuf->mtype = type;
    qbuf->honeyBee = honey;
    if((msgsnd(qid, (struct msgbuf *)qbuf, sizeof(int), 0)) ==-1)
    {
        printf("can't send message\n");
        //perror("msgsnd");
        exit(1);
    }

}
