#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define INPUTFILE "input.txt"
#define WORDLEN 500
#define COUNTSTREAM 4

int main(int argc, char *argv[])
{
    char name[WORDLEN], str[WORDLEN], s[COUNTSTREAM][WORDLEN];
    FILE *in;
    int *listPid, i, status, stat, num=1;

    in=fopen(INPUTFILE, "r");
    if(in==NULL)
    {
        printf("no file");
        return 1;
    }
    fscanf(in,"%s",name);
    fgets(str,WORDLEN,in);
    fgets(str,WORDLEN,in);
    fclose(in);
    //printf("%s",str);
    in=fopen(name,"r");
    if(in==NULL)
    {
        printf("нет указанного файла");
        return 1;
    }
    listPid=(int*)calloc(COUNTSTREAM,sizeof(int));

    for (i=0; i<COUNTSTREAM; i++)
        fgets(s[i],WORDLEN,in);

    while( !feof(in) )
    {
        for (i=0; i<COUNTSTREAM; i++)
        {
            listPid[i]=fork();
            if (-1 == listPid[i]) {
                perror("fork"); // произошла ошибка
                exit(1); //выход из родительского процесса
            } else if (0 == listPid[i])
            {
                fclose(in);
                free(listPid);
                if( strcmp(s[i],str) == 0 )
                {
                    printf("Потомок %d нашел указанную строку. Ее номер %d\n",i+num,num+i);
                    exit(num+i);
                }
                //printf("Потомок %d не нашел указанную строку\n",i+num);
                exit(0);
            }
        }
        for (i=0; i<COUNTSTREAM; i++)
        {
            fgets(s[i],WORDLEN,in);
            status = waitpid(listPid[i], &stat, 0);
            if (listPid[i] == status) {
                printf("процесс-потомок %d, результат=%d\n", i+num, WEXITSTATUS(stat));
            }
            listPid[i]=0;
        }
        num+=COUNTSTREAM;
    }
	fclose(in);
	free(listPid);
    return 0;
}





