#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define INPUTFILE "input.txt"
#define WORDLEN 500

char **ReadData( char **s, int *count)
{
    char buf[WORDLEN],kol[WORDLEN];
    int i, n;
    FILE *in;
    in=fopen(INPUTFILE,"r");
    if (in == NULL)
    {
        printf("no file");
        return NULL;
    }
    fgets(buf,WORDLEN,in);
    n = strlen(buf);
    buf[n-1]='\0';
    fgets(kol,WORDLEN,in);
    *count=atoi(kol);
    if ( *count < 1 )
    {
        printf("nothing look");
        return NULL;
    }
    if ( n < 1 )
    {
        printf("empty file");
        return NULL;
    }
    s=(char**)calloc(*count+1,sizeof(char*));
    s[0]=(char*)calloc(n,sizeof(char));
    strcpy(s[0],buf);

    for (i=1; i<=*count; i++)
    {

        fgets(buf,500,in);
        n = strlen(buf);

        s[i]=(char*)calloc(n-1,sizeof(char));
        strcpy(s[i],buf);
    }
    return s;

}

int FindStr(char *fileName, char *str)
{
	FILE *in;
	char buf[WORDLEN];
	int num=1;
	in=fopen(fileName,"r");
	if( in==NULL)
	{
		//printf("нет указанного файла");
		return -1;
	}
	fgets(buf,WORDLEN,in);
	while( !feof(in) )
	{
		if (strcmp(buf,str) == 0)
		{
			fclose(in);
			return num;
		}
		num++;
		fgets(buf,WORDLEN,in);
	}
	fclose(in);
	return 0;
}
	
void FreeMem(char **s, int *p, int n)
{
	int i=0;
	for(i=0; i<=n; i++)
	{
		free(s[i]);
	}
	free(s);
	free(p);
}

int main(int argc, char *argv[])
{
    char **list;
    int *listPid, i, count, status, stat, res;
    list=ReadData(list,&count);
    listPid=(int*)calloc(count,sizeof(int));
    printf("kol=%d\n",count);
    for (i=1; i <= count; i++)
    {
		listPid[i-1]=fork();
		if (-1 == listPid[i-1]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == listPid[i-1]) 
        {
			res=FindStr(list[0],list[i]);
			FreeMem(list, listPid, count);
			switch(res)
			{
				case -1: 
				printf("Потомок %d не открыл указанный файл\n",i);
				exit(-1);
				case 0:
				printf("Потомок %d строку не нашел\n",i);
				exit(0);
				default:
				printf("Потомок %d нашел указанную строку. Ее номер %d\n",i,res);
				exit(res);
					
			}
        }
    }
    for (i=0; i<count; i++)
    {
		status = waitpid(listPid[i], &stat, 0);
        if (listPid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i+1, WEXITSTATUS(stat));
        }
    }
	FreeMem(list,listPid,count);
    return 0;


}
