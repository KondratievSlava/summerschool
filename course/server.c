#include <unistd.h>
#include <netinet/in.h>
#include <netdb.h>
#include "myqueue1.h"
int const SERVER_PORT_TCP = 50231;
int const SERVER_PORT_UDP = 60231;
int const CLIENT1_PORT_UDP = 61231;
int const CLIENT2_PORT_UDP = 61241;
int const CLIENT1_PORT = 50232;
int const CLIENT2_PORT = 50233;
int const MAX_PORT = 6;
char const SERVER_NAME[16] = "127.0.0.1";
char const CLIENT_NAME[16] = "127.255.255.255";
int const SERVER_READY = 1;
int const SERVER_ANSWER = 2;
int const MAX_MES = 8;
int const WAIT_L = 5;
int const WAIT_K = 2;
int const CLIENT_TCP1 = 1;
int const CLIENT_TCP2 = 2;



// Функция обработки ошибок
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int nclients = 0;

// макрос для печати количества активных
// пользователей
void printusers()
{
    if(nclients)
    {
        printf("%d user on-line\n",nclients);
    }
    else
    {
        printf("No User on line\n");
    }
}

int init_server(int argc, char *argv[])
{
    int sockfd; // дескрипторы сокетов
    int portno; // номер порта


    struct sockaddr_in serv_addr; // структура сокета сервера и клиента
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // ошибка при создании сокета
    if (sockfd < 0)
        error("ERROR opening socket");

    // Шаг 2 - связывание сокета с локальным адресом
    memset((char *) &serv_addr, '\0', sizeof(serv_addr));
    portno = SERVER_PORT_TCP; //atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    serv_addr.sin_port = htons(portno);
    // вызываем bind для связывания
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    return sockfd;
}

int server_udp_L(int sock, int qid, int argc, char *argv[])
{
    //настройка udp сокета
    struct sockaddr_in addr;
    struct hostent *server;
    char buf[WORD_LEN];
    int i;
    i = 0;
	if(argc > 1)
	{
		server = gethostbyname(argv[1]);
	}
	else
	{
		server = gethostbyname(CLIENT_NAME);
	}
    memset((char *) &addr, '\0', sizeof(addr));
    addr.sin_family = AF_INET;
    memcpy((char *)server->h_name,
           (char *)&addr.sin_addr.s_addr,
           server->h_length);
    addr.sin_port = htons(CLIENT2_PORT_UDP);

    strcpy(buf, "Есть сообщение\n");
    int j;
    while(i < 3)
    {
		j = 0;
        sleep(WAIT_L);
        //узнаем количество сообщений в очереди
        if(sum_mes_que(qid) > 0)
        {
            /* Send the message in buf to the server */
            while(j <= MAX_PORT)
			{
				addr.sin_port = htons(CLIENT2_PORT_UDP + j);
				if(sendto(sock, &buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr)) <= 0)
				{
					error("something went wrong..");

					return EXIT_FAILURE;
				}
				j++;
			}
            printf("send mess2 udp\n");
        }
        //i++;
    }
    return 0;
}

int server_udp_K(int sock, int qid, int argc, char *argv[])
{
    //настройка сокета udp
    struct sockaddr_in addr;
    struct hostent *server;
    char buf[WORD_LEN];
    int i;
    i = 0;
	if(argc > 1)
	{
		server = gethostbyname(argv[1]);
	}
	else
	{
		server = gethostbyname(CLIENT_NAME);
	}
    memset((char *) &addr, '\0', sizeof(addr));
    addr.sin_family = AF_INET;
    memcpy((char *)server->h_name,
           (char *)&addr.sin_addr.s_addr,
           server->h_length);
    addr.sin_port = htons(CLIENT1_PORT_UDP);
    strcpy(buf, "Жду сообщений\n");
	int j;
	
    while(i < 3)
    {
		j = 0;
        sleep(WAIT_K);
        //узнаем количество сообщений в очереди
        if(sum_mes_que(qid) < MAX_MES)
        {
            /* Send the message in buf to the server */
            while(j <= MAX_PORT)
			{
				addr.sin_port = htons(CLIENT1_PORT_UDP + j);
				if(sendto(sock, &buf, strlen(buf), 0, (struct sockaddr *)&addr, sizeof(addr)) <= 0)
				{
					error("something went wrong..");

					return EXIT_FAILURE;
				}
				j++;
			}
            printf("send mess1 udp\n");
        }
        //i++;
    }
    return 0;
}

void client2(int sock, int qid) //мы отправляем ему сообщения
{
	uint8_t buf[MAX_MSG_SIZE];
	void *vbuf;
	Packet mess = PACKET__INIT;
	struct mymsgbuf *mes;
	int len;
    struct mymsgbuf qbuf;
    long qtype = sizeof(struct mymsgbuf) - sizeof(long);
    printf("in 2 serv\n");
    if(sum_mes_que(qid) > 0)
    {
		mes = read_message(qid, &qbuf, qtype);
	
		printf("serv send mes: t = %d len = %d\ns = %s\n", mes->time, mes->len, mes->str);
		mess.time = mes->time;
		mess.name.len = mes->len;
		mess.name.data = mes->str;
		mess.str = malloc(WORD_LEN);
		strcpy(mess.str, mes->str);
        //отправка сообщения
        //упаковка структуры
        len = packet__get_packed_size(&mess);
		vbuf = malloc(len);
        packet__pack(&mess,  vbuf);
        send(sock, vbuf, len, 0);
    }
    else
    {
        printf("aaa\n");
        send(sock, &buf, 0, 0);
        printf("serv send err: aaaaa\n");
	}

    nclients--; // уменьшаем счетчик активных клиентов
    printf("-disconnect 2\n");
    printusers();
    return;

}

void client1(int sock, int qid) //присылает сообщения нам
{
	printf("in 1 cerv\n");
	Packet *mes;
	uint8_t buf[MAX_MSG_SIZE];
	struct mymsgbuf qbuf;
    int bytes_recv, k;
    bytes_recv = recv(sock, &buf, sizeof(buf), 0);
    if(bytes_recv < 0) error("ERR reading socket");
    mes = packet__unpack(NULL, bytes_recv, buf);
	printf("serv get mes: t = %d len = %d\ns = %s\n", mes->time, (int)mes->name.len, mes->name.data);
    //------
    //отправляем сообщение в очередь
    long qtype = sizeof(struct mymsgbuf) - sizeof(long);//WORD_LEN;
    k = sum_mes_que(qid);
    if (k < MAX_MES)
    {
		send_message(qid, (struct mymsgbuf *)&qbuf, qtype, mes);
	}
    //-----
    nclients--; // уменьшаем счетчик активных клиентов
    printf("-disconnect 1\n");
    printusers();
    return;
}

int init_udp(int argc, char *argv[])
{
    int sockfd; // дескрипторы сокетов
    int portno; // номер порта
    struct sockaddr_in serv_addr; // структура сокета сервера и клиента
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    // ошибка при создании сокета
    if (sockfd < 0)
        error("ERROR opening socket");

    // Шаг 2 - связывание сокета с локальным адресом
    memset((char *) &serv_addr, '\0', sizeof(serv_addr));
    portno = SERVER_PORT_UDP; //atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    serv_addr.sin_port = htons(portno);
    // вызываем bind для связывания
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    return sockfd;
}

int init_queue()
{
    int msgqid;
    printf("create queue\n");
    struct msqid_ds info;
    key_t key;
    key = ftok(".", 'm');
    if((msgqid = msgget(key, IPC_CREAT|0660)) == -1)
    {
        printf("can't create queue\n");
        perror("msgget");
        exit(1);
    }
    msgctl(msgqid, IPC_STAT, &info);
    printf("mes in qu = %d\n", (int)info.msg_qnum);
    if( (int)info.msg_qnum > 0)
    {

        msgctl(msgqid, IPC_RMID, &info);
        if((msgqid = msgget(key, IPC_CREAT|0660)) == -1)
        {
            printf("can't create queue\n");
            perror("msgget");
            exit(1);
        }
		msgctl(msgqid, IPC_STAT, &info);
		printf("mes in qu = %d\n", (int)info.msg_qnum);
    }
    return msgqid;
}



int main(int argc, char *argv[])
{
	int msgqid;
    struct sockaddr_in cli_addr;
    socklen_t clilen; // размер адреса клиента типа socklen_t
    int sock_tcp, newsockfd, sock_udp, flag_udp, flag_mes, n;
    int pid; // id номер потока
	//создание очереди
    msgqid = init_queue();
    //создание процессов, которые осуществляют UDP рассылку
    sock_udp = init_udp(argc, argv);
    pid = fork(); //создание процесса рассылки udp K
    if (pid < 0)
        error("ERROR on fork");
    if (pid == 0)
    { //закрыли рассылку для 1 клиента
		flag_udp = server_udp_K(sock_udp, msgqid, argc, argv);
		if (flag_udp < 0)
			error("ERROR in UDP K");
		close(sock_udp);
		return 0;

    }
    pid = fork(); //создание процесса рассылки udp L
    if (pid < 0)
        error("ERROR on fork");
    if (pid == 0)
    { //закрыли рассылку для 2 клиента
		flag_udp = server_udp_L(sock_udp, msgqid, argc, argv);
		if (flag_udp < 0)
			error("ERROR in UDP L");
		close(sock_udp);
		return 0;

    }
    close(sock_udp);
    sock_tcp = init_server(argc, argv);
    // Шаг 3 - ожидание подключений, размер очереди - 5
    listen(sock_tcp,5);
    clilen = sizeof(cli_addr);
    //---------
    // Шаг 4 - извлекаем сообщение из очереди
    // цикл извлечения запросов на подключение из очереди
    while (1)
    {
        newsockfd = accept(sock_tcp,(struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");
        // подключившихся клиентов
        // вывод сведений о клиенте
        pid = fork();
        if (pid < 0)
            error("ERROR on fork");
        if (pid == 0)
        {
            close(sock_tcp);
            nclients++; // увеличиваем счетчик
            printusers();
            n = recv(newsockfd, &flag_mes, sizeof(flag_mes), 0);
            printf("serv: port cli = %d, type client = %d\n",cli_addr.sin_port, flag_mes);
            if(flag_mes == CLIENT_TCP1)
                client1(newsockfd, msgqid);
            if(flag_mes == CLIENT_TCP2)
                client2(newsockfd, msgqid);
            close(newsockfd);
            exit(0);
        }
        else 
        {
			close(newsockfd);
		}
    } /* end of while */
    close(sock_tcp);
    return 0; /* we never get here */
}

