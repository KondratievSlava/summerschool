#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include "messege.pb-c.h"
#include <pthread.h>
#include <errno.h>
#define MAX_MSG_SIZE 1024
#define input "input.txt"
char const SERVER_NAME[16] = "127.0.0.1";
char const MES1[30] = "Жду сообщений\n";
char const MES2[32] = "Есть сообщение\n";
int const CLIENT_PORT_TCP = 50232;
int const CLIENT_PORT_UDP = 61241;
int const SERVER_PORT_UDP = 60231;
int const SERVER_PORT_TCP = 50231;
int const MAX_STRING = 15;
int const CLIENT_TCP1 = 1;
int const CLIENT_TCP2 = 2;
int const WAIT_T = 6;
int const WORD_LEN = 100;
int const MAX_PORT = 6;

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int init_udp()
{
    int sockfd; // дескрипторы сокетов
    int portno, i; // номер порта
    int pruf_port;
    struct sockaddr_in serv_addr; // структура сокета сервера и клиента
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    // ошибка при создании сокета
    if (sockfd < 0)
        error("ERROR opening socket");
    // Шаг 2 - связывание сокета с локальным адресом
    memset((char *) &serv_addr, '\0', sizeof(serv_addr));
    
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    portno = CLIENT_PORT_UDP; //atoi(argv[1]);
    serv_addr.sin_port = htons(portno);
    pruf_port = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
    i = 1;
    // вызываем bind для связывания
    while(pruf_port < 0 && i <= MAX_PORT)
    {
		portno = CLIENT_PORT_UDP + i; //atoi(argv[1]);
		serv_addr.sin_port = htons(portno);
		pruf_port = bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr));
		i++;
	}
	if(i > MAX_PORT)
        error("ERROR on binding");
    return sockfd;
}

int connect_TCP_2(struct sockaddr_in addr)
{
    uint8_t buf[MAX_MSG_SIZE];
    Packet *mes;
    int sock_tcp, bytes;
    int t;
    t = rand() % WAIT_T;
    sock_tcp = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_tcp < 0)
        error("ERROR opening socket");
    addr.sin_port = htons(SERVER_PORT_TCP);
    if (connect(sock_tcp,(struct sockaddr *) &addr,sizeof(addr)) < 0)
        error("ERROR connecting");

    send(sock_tcp, &CLIENT_TCP2, sizeof(CLIENT_TCP1), 0);
    bytes = recv(sock_tcp, buf, sizeof(buf), 0);
    if(bytes < 0) error("ERR reading socket");
    if(bytes > 0)
    {
        mes = packet__unpack(NULL, bytes, buf);
        printf("cl get mes: t = %d len = %d\ns = %s\n", mes->time, (int)mes->name.len, mes->name.data);
    }
    close(sock_tcp);
    sleep(t);
    return 0;
}

int main(int argc, char *argv[])
{
	
	printf("client 2 run\n");
    char buf[100];
    int sock_udp, n;
    struct sockaddr_in serv_addr;
    socklen_t size = sizeof(serv_addr);
    time_t t;
    srand((unsigned) time(&t));
    sock_udp = init_udp();
    while(1)
    {
        n = recvfrom(sock_udp, (char *)buf, sizeof(char[100]), 0, (struct sockaddr *)&serv_addr, &size);
        buf[n] = '\0';
        printf("cl get mes: %s", buf);
        if(strcmp(buf, MES2) == 0)
        {
            printf("connect\n");
            connect_TCP_2(serv_addr);
        }
    }
    return 0;
}
