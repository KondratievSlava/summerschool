/* Пример простого TCP сервера
   Порт является аргументом, для запуска сервера неободимо ввести:
   # ./[имя_скомпилированного_файла] [номер порта]
   # ./server 50231
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

int const SERVER_PORT = 50231;
char const SERVER_NAME[16] = "127.0.0.1";
int const SERVER_READY = 1;
int const SERVER_ANSWER = 2;
int const MAX_WAYS = 8;

struct ways
{
    int len;
    int num;
};
typedef struct ways Ways;
// прототип функции, обслуживающей
// подключившихся пользователей
void plane(int);

// Функция обработки ошибок
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

// глобальная переменная – количество
// активных пользователей
int nclients = 0;

// макрос для печати количества активных
// пользователей
void printusers()
{
    if(nclients)
    {
        printf("%d user on-line\n",nclients);
    }
    else
    {
        printf("No User on line\n");
    }
}

int min( int a, int b)
{
    if (a < b) return a;
    else return b;
}

int compare1(const void *x1, const void *x2)
{
    Ways a = *(Ways*) x1;
    Ways b = *(Ways*) x2;
    return (b.len - a.len);
}

Ways *search_priority_ways(Ways *list_plane, int n, int m, int x, int y) //поиск наиболее длинных направлений
{
    list_plane = malloc(8 * sizeof(Ways));

    list_plane[0].len = m - x;
    list_plane[0].num = 1;

    list_plane[1].len = min(m - x, y + 1);
    list_plane[1].num = 2;

    list_plane[2].len = y + 1;
    list_plane[2].num = 3;

    list_plane[3].len = min(y + 1, x + 1);
    list_plane[3].num = 4;

    list_plane[4].len = x + 1;
    list_plane[4].num = 5;

    list_plane[5].len = min(x + 1, n - y);
    list_plane[5].num = 6;

    list_plane[6].len = n - y;
    list_plane[6].num = 7;

    list_plane[7].len = min(m - x, n - y);
    list_plane[7].num = 8;

    qsort(list_plane, 8, sizeof(Ways), compare1);

    return list_plane;
}

int code_path(int deg, int *hx, int *hy)
{
    switch(deg)
    {
    case 1: //вправо
        *hx = 1;
        *hy = 0;
        break;
    case 2: //вправо-вверх
        *hx = 1;
        *hy = -1;
        break;
    case 3: //вверх
        *hx = 0;
        *hy = -1;
        break;
    case 4: //влево-вверх
        *hx = -1;
        *hy = -1;
        break;
    case 5: //влево
        *hx = -1;
        *hy = 0;
        break;
    case 6: //влево-вниз
        *hx = -1;
        *hy = 1;
        break;
    case 7: //вниз
        *hx = 0;
        *hy = 1;
        break;
    case 8: //вправо-вниз
        *hx = 1;
        *hy = 1;
        break;

    default:
        return -1;
    }
    return 0;

}


int main(int argc, char *argv[])
{
    int sockfd, newsockfd; // дескрипторы сокетов
    int portno; // номер порта
    int pid; // id номер потока
    socklen_t clilen; // размер адреса клиента типа socklen_t
    struct sockaddr_in serv_addr, cli_addr; // структура сокета сервера и клиента
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    // ошибка при создании сокета
    if (sockfd < 0)
        error("ERROR opening socket");

    // Шаг 2 - связывание сокета с локальным адресом
    //bzero((char *) &serv_addr, sizeof(serv_addr));
    memset((char *) &serv_addr, '\0', sizeof(serv_addr));
    portno = SERVER_PORT; //atoi(argv[1]);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY; // сервер принимает подключения на все IP-адреса
    serv_addr.sin_port = htons(portno);
    // вызываем bind для связывания
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
        error("ERROR on binding");
    // Шаг 3 - ожидание подключений, размер очереди - 5
    listen(sockfd,5);
    clilen = sizeof(cli_addr);

    // Шаг 4 - извлекаем сообщение из очереди
    // цикл извлечения запросов на подключение из очереди
    printf("begin cickl\n");
    while (1)
    {
        newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr, &clilen);
        if (newsockfd < 0)
            error("ERROR on accept");

        nclients++; // увеличиваем счетчик
        // подключившихся клиентов
        /*
        // вывод сведений о клиенте
        struct hostent *hst;
        hst = gethostbyaddr((char *)&cli_addr.sin_addr, 4, AF_INET);
        printf("+%s [%s] new connect!\n",
        (hst) ? hst->h_name : "Unknown host",
        (char*)inet_ntoa(cli_addr.sin_addr));*/
        printusers();

        pid = fork();
        if (pid < 0)
            error("ERROR on fork");
        if (pid == 0)
        {
            close(sockfd);
            plane(newsockfd);
            close(newsockfd);
            exit(0);
        }
        else close(newsockfd);
    } /* end of while */
    close(sockfd);
    return 0; /* we never get here */
}

void plane (int sock)
{
	Ways *list_plane = NULL;
    int n, m, x, y, k, i, j, **mas, hy, hx, x0, y0;
    //int n,a,b;
    int bytes_recv; // размер принятого сообщения

    // отправляем клиенту сообщение
    write(sock, &SERVER_READY, sizeof(int));

    // получение данных
    bytes_recv = read(sock,&n,sizeof(n));
    if (bytes_recv < 0) error("ERROR reading from socket");
    bytes_recv = read(sock,&m,sizeof(m));
    if (bytes_recv < 0) error("ERROR reading from socket");
    bytes_recv = read(sock,&x,sizeof(x));
    if (bytes_recv < 0) error("ERROR reading from socket");
    bytes_recv = read(sock,&y,sizeof(y));
    if (bytes_recv < 0) error("ERROR reading from socket");
    bytes_recv = read(sock,&k,sizeof(k));
    if (bytes_recv < 0) error("ERROR reading from socket");
	
	mas = malloc(n * sizeof(int*));
    for(i = 0; i < n; i++)
    {
		mas[i] = malloc(m * sizeof(int));
        for(j = 0; j < m; j++)
        {
			bytes_recv = read(sock,&mas[i][j],sizeof(mas[i][j]));
			if (bytes_recv < 0) error("ERROR reading from socket");
        }
    }
    /* //вывод данных
    printf("x=%d y=%d n=%d m=%d k=%d\n", x, y, n, m, k);
	for(i = 0; i < n; i++)
    {
        for(j = 0; j < m; j++)
        {
			printf("%d ", mas[i][j]);
        }
        printf("\n");
    }*/
    //вычиления-----
	list_plane = search_priority_ways(list_plane, n, m, x, y);
	for(i = 0; i < k; i++)
	{
		
		code_path(list_plane[i].num, &hx, &hy); //корректность направления
		x0 = x;
		y0 = y;
		list_plane[i].len = 0;
		while (x0 < m && x0 >= 0  && y0 >= 0 && y0 < n) //пока не вышли за границу
		{
			list_plane[i].len += mas[y0][x0];
			//printf("x=%d y=%d r=%d hx=%d hy=%d\n",x0,y0,mas[y0][x0], hx, hy);
			x0 += hx;
			y0 += hy;
		}
	}
    //------
    //отправка ответа-----
    write(sock, &SERVER_ANSWER, sizeof(int));
    for(i = 0; i < k; i++)
    {
		send(sock, &list_plane[i].len, sizeof(int), 0);
		send(sock, &list_plane[i].num, sizeof(int), 0);
	}
    //---------
	for(i = 0; i < n; i++)
		free(mas[i]);
	free(mas);
	free(list_plane);
    nclients--; // уменьшаем счетчик активных клиентов
    printf("-disconnect\n");
    printusers();
    return;
}
