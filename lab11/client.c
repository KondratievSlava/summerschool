#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


#include <pthread.h>
#include <errno.h>

#define input "input.txt"
char const SERVER_NAME[16] = "127.0.0.1";
int const CLIENT_PORT = 50231;
int const MAX_WAYS = 8;


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int **init_data(int *n, int *m, int *x, int *y, int *k) //ввод кол-ва самолетов, поля и его размеры
{
    FILE *in;
    int i, j, n1, m1, **mas;
    in = fopen(input, "r");
    fscanf(in, "%d%d%d%d%d", n, m, x, y, k);
    if (*k > MAX_WAYS)
    {
        printf("max count ways = 8\n");
        *k = MAX_WAYS;
    }
    if (*x > *n)
    {
        *x = *n / 2;
    }
    if (*y > *m)
    {
        *y = *m / 2;
    }
    *x = *x - 1;
    *y = *y - 1;
    n1 = *n;
    m1 = *m;
    //printf("%d %d\n", n1, m1);
    mas = malloc( sizeof(int *) * n1);
    for (i = 0; i < n1; i++)
    {
        mas[i] = malloc( sizeof(int) * m1);
        for (j = 0; j < m1; j++)
        {
            fscanf(in, "%d", &mas[i][j]);
        }
    }
	return mas;
}

int main(int argc, char *argv[])
{
    int i, j, n, nn, m, x, y, k, flag = 1, res, choice, **mas;
    int my_sock, portno, bytes_recv;
    struct sockaddr_in serv_addr;
    struct hostent *server;
    
    mas = init_data(&n, &m, &x, &y, &k);
    printf("TCP DEMO CLIENT\n");

    // извлечение порта
    portno = CLIENT_PORT; 

    // Шаг 1 - создание сокета
    my_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (my_sock < 0)
        error("ERROR opening socket");
    // извлечение хоста
    server = gethostbyname(SERVER_NAME); // gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    // заполенние структуры serv_addr
    memset((char *) &serv_addr, '\0', sizeof(serv_addr));
    //bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    
    //memcpy((char *)server->h_addr,
    memcpy((char *)server->h_name,
			(char *)&serv_addr.sin_addr.s_addr,
          server->h_length);
   /* bcopy((char *)server->h_addr,
          (char *)&serv_addr.sin_addr.s_addr,
          server->h_length);*/
    // установка порта
    serv_addr.sin_port = htons(portno);

    // Шаг 2 - установка соединения
    if (connect(my_sock,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0)
        error("ERROR connecting");

    printf("begin cickl\n");
    // Шаг 3 - чтение и передача сообщений
    while (flag)
    {
        nn = recv(my_sock, &choice, sizeof(choice), 0);
        if (nn > 0)
        {
            switch(choice)
            {
            case 1:

                // передаем параметры клиента серверу
                //send(my_sock, &buff[0], strlen(&buff[0]), 0);
                send(my_sock, &n, sizeof(n), 0);
                send(my_sock, &m, sizeof(m), 0);
                send(my_sock, &x, sizeof(x), 0);
                send(my_sock, &y, sizeof(y), 0);
                send(my_sock, &k, sizeof(k), 0);
                for(i = 0; i < n; i++)
                {
					for(j = 0; j < m; j++)
					{
						send(my_sock, &mas[i][j], sizeof(int), 0);
					}
				}
                break;
            case 2:
                printf("answer\n");
                for(i = 0; i < k; i++)
                {
					printf("plane %d\n", i + 1);
					bytes_recv = read(my_sock, &res, sizeof(res));
					if (bytes_recv < 0) error("ERROR reading from socket");
					printf("res = %d\n", res);
					bytes_recv = read(my_sock, &res, sizeof(res));
					if (bytes_recv < 0) error("ERROR reading from socket");
					printf("num way = %d\n", res);
				}
				flag = 0;
                break;
            default:
                n = 0;
            }
        }
    }
    for(i = 0; i < n; i++)
		free(mas[i]);
	free(mas);
	//free(server);
    printf("client end\n");
    close(my_sock);
    return 0;
}
