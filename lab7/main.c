#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define INPUTFILE "input.txt"
#define WORDLEN 500
#define COUNTSTREAM 4
#define FIFO1 "fifo.txt"

int minn(int a, int b)
{
	if (a < b) 
		return a;
		
	return b;
}

int main(int argc, char *argv[])
{
    char name[WORDLEN], str[WORDLEN], s[COUNTSTREAM][WORDLEN], buf[WORDLEN], aaaa[WORDLEN];
    FILE *in, *icp;
    int *listPid, i, status, stat, num = 1, countStr = COUNTSTREAM;

    mkfifo(FIFO1, 0666);
    in = fopen(INPUTFILE, "r");
    if(in == NULL)
    {
        printf("no file");
        return 1;
    }
    fscanf(in,"%s",name);
    fgets(str,WORDLEN,in);
    fgets(str,WORDLEN,in);
    fclose(in);
    //printf("%s",str);
    in = fopen(name,"r");
    if(in == NULL)
    {
        printf("нет указанного файла");
        return 1;
    }
    listPid=(int*)calloc(COUNTSTREAM,sizeof(int));

    for (i = 0; i < COUNTSTREAM; i++)
        fgets(s[i],WORDLEN,in);

    while( !feof(in) )
    {
        for (i = 0; i < minn(countStr,COUNTSTREAM); i++)
        {
            listPid[i] = fork();
            if (-1 == listPid[i]) {
                perror("fork"); // произошла ошибка
                exit(1); //выход из родительского процесса
            } else if (0 == listPid[i])
            {
				
                fclose(in);
                free(listPid);
                if( strcmp(s[i],str) == 0 )
                {
                    printf("Потомок %d нашел указанную строку. Ее номер %d\n",i + num, num + i);
                    icp = open(FIFO1, O_WRONLY, 0);
                    strcmp(buf,"Потомок ");
                    sprintf(aaaa, "%d", i + num);
                    strcmp(buf, aaaa);
                    strcmp(buf, " нашел указанную строку\n");
                    write(icp, buf, WORDLEN);
                    close(icp);
                    //exit(num + i);
                }
               // printf("Потомок %d не нашел указанную строку\n",i+num);
                exit(0);
            }
        }
        countStr = 0;
        for (i = 0; i < COUNTSTREAM; i++)
        {
            if ( (fgets(s[i],WORDLEN,in)) > 0)
				countStr++;
            //printf("child = %d\n",i);

            //status = waitpid(listPid[i], &stat, 0);
            /*f (listPid[i] == status) {
                printf("процесс-потомок %d, результат=%d\n", i+num, WEXITSTATUS(stat));
            }
            listPid[i]=0;*/
        }

        num += COUNTSTREAM;
        
    }
    fclose(in);
    free(listPid);
    
   /* for (i = 0; i < minn(countStr, COUNTSTREAM); i++)
    {
		status = waitpid(listPid[i], &stat, 0);
		//printf("завершение потомка %d\n",i);
	}*/
	sleep(1);
    
    icp = open(FIFO1, O_RDONLY, 0);
    while ( (num = read(icp, str, WORDLEN)) > 0)
    {
        printf("%s",str);
        fflush(stdout);
	}
    close(icp);
    
    return 0;
}





