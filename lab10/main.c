#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <unistd.h>

#define input "input.txt"

int n, m, x, y, k, **mas;
int const MAX_WAYS = 8;

struct ways
{
    int len;
    int num;
};
typedef struct ways Ways;

void init_data() //ввод кол-ва самолетов, поля и его размеры
{
    FILE *in;
    int i, j, n1, m1;
    in = fopen(input, "r");
    fscanf(in, "%d%d%d%d%d", &n, &m, &x, &y, &k);
    if (k > MAX_WAYS)
    {
        printf("max count ways = 8\n");
        k = MAX_WAYS;
    }
    if (x > n)
    {
        x = n / 2;
    }
    if (y > m)
    {
        y = m / 2;
    }
    x = x - 1;
    y = y - 1;
    n1 = n;
    m1 = m;
    //printf("%d %d\n", n1, m1);
    mas = malloc( sizeof(int *) * n1);
    for (i = 0; i < n1; i++)
    {
        mas[i] = malloc( sizeof(int) * m1);
        for (j = 0; j < m1; j++)
        {
            fscanf(in, "%d", &mas[i][j]);
        }
    }

}

void print_data(Ways *list) //вывод поля
{
    int i, j;
    printf("n=%d m=%d\nx=%d y=%d\n",n, m, x, y);
    for (i = 0; i < n; i++)
    {
        for (j = 0; j < m; j++)
        {
            printf("%d ", mas[i][j]);
        }
        printf("\n");
    }
    /*printf("ways\n");
    for (i = 0; i < 8; i++)
    {
        printf("%d %d\n",list[i].len, list[i].num);
    }*/
    //scanf("%d", &i);
}

int code_path( int deg, int *hx, int *hy)
{
    switch(deg)
    {
    case 1: //вправо
        *hx = 1;
        *hy = 0;
        break;
    case 2: //вправо-вверх
        *hx = 1;
        *hy = -1;
        break;
    case 3: //вверх
        *hx = 0;
        *hy = -1;
        break;
    case 4: //влево-вверх
        *hx = -1;
        *hy = -1;
        break;
    case 5: //влево
        *hx = -1;
        *hy = 0;
        break;
    case 6: //влево-вниз
        *hx = -1;
        *hy = 1;
        break;
    case 7: //вниз
        *hx = 0;
        *hy = 1;
        break;
    case 8: //вправо-вниз
        *hx = 1;
        *hy = 1;
        break;

    default:
        return -1;
    }
    return 0;

}

int min( int a, int b)
{
    if (a < b) return a;
    else return b;
}

int compare1(const void *x1, const void *x2)
{
    Ways a = *(Ways*) x1;
    Ways b = *(Ways*) x2;
    return (b.len - a.len);
}

Ways *search_priority_ways(Ways *list_plane) //поиск наиболее длинных направлений
{
    list_plane = malloc(8 * sizeof(Ways));

    list_plane[0].len = m - x;
    list_plane[0].num = 1;

    list_plane[1].len = min(m - x, y + 1);
    list_plane[1].num = 2;

    list_plane[2].len = y + 1;
    list_plane[2].num = 3;

    list_plane[3].len = min(y + 1, x + 1);
    list_plane[3].num = 4;

    list_plane[4].len = x + 1;
    list_plane[4].num = 5;

    list_plane[5].len = min(x + 1, n - y);
    list_plane[5].num = 6;

    list_plane[6].len = n - y;
    list_plane[6].num = 7;

    list_plane[7].len = min(m - x, n - y);
    list_plane[7].num = 8;

    qsort(list_plane, 8, sizeof(Ways), compare1);

    return list_plane;
}

void *plane(void *arg)
{
	
	int deg = *(int *)arg;
	//printf("in plane %d\n", deg);
    int res = 0, hy, hx, x0, y0;
    Ways *resl;
    resl = malloc(sizeof(Ways));
    /*int flag;
    flag = */
    code_path(deg, &hx, &hy); //корректность направления
    x0 = x;
    y0 = y;
    /*if (flag == -1)
        pthread_exit((void*)res);*/
    while (x0 < m && x0 >= 0  && y0 >= 0 && y0 < n) //пока не вышли за границу
    {
        res += mas[y0][x0];
        //printf("x=%d y=%d r=%d hx=%d hy=%d\n",x0,y0,mas[y0][x0], hx, hy);
        x0 += hx;
        y0 += hy;
    }
    resl->len = res;
    resl->num = deg;
    //printf("out plane %d\n", deg);
	pthread_exit((void*)resl);
    //return res;
}

int main()
{
    Ways *list_plane = NULL;
    Ways *res;
    int  i, result;
    pthread_t *thread;
    void *status[MAX_WAYS];

    init_data();
    res = malloc(sizeof(Ways));
    thread = malloc(k * sizeof(pthread_t));
    //results = malloc(k * sizeof(Ways));
    list_plane = search_priority_ways(list_plane);

    print_data(list_plane);

    for (i = 0; i < k; i++) //создание потоков
    {
		//printf("%d thread\n", i + 1);
        result = pthread_create(&thread[i], NULL, plane, &list_plane[i].num);
        if (result != 0)
        {
            perror("Creating thread\n");
            return EXIT_FAILURE;
        }
    }

    for (i = 0; i < k; i++) //вывод результатов
    {
		//printf("%d out thread\n", i + 1);
        result = pthread_join(thread[i], &status[i]);
        if (result != 0) 
        {
            perror("Joining the first thread");
            return EXIT_FAILURE;
        }
	}
	for (i = 0; i < k; i++)
	{
		res = ((Ways*)status[i]);
		printf("plane %d\n", i + 1);
		printf("num way = %d\n", res->num);
		printf("res = %d\n", res->len );

        //res = plane(list_plane[4].num); //запуск самолета
        //printf("num way = %d\nres = %d\n",list_plane[4].num, res);

        
    }
	return 0;
}
