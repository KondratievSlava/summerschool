#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/tty.h> 

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kondratiev");

int init_module(void);
void cleanup_module(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "chardev"   /* Имя устройства, будет отображаться в /proc/devices   */
#define BUF_LEN 80                      /* Максимальная длина сообщения */

static int Major;             /* Старший номер устройства нашего драйвера */
static int Device_Open = 0;   /* Устройство открыто?
                               * используется для предотвращения одновременного
                               * обращения из нескольких процессов */
static char msg[BUF_LEN];     /* Здесь будет собираться текст сообщения */
static char *msg_Ptr;
static char msg_get[BUF_LEN];

static struct file_operations fops = {
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};

int init_module(void)
{
    Major = register_chrdev(0, DEVICE_NAME, &fops);

    if (Major < 0) {
        printk("Registering the character device failed with %d\n",
               Major);
        return Major;
    }

    printk("<1>I was assigned major number %d.  To talk to\n", Major);
    printk("<1>the driver, create a dev file with\n");
    printk("'mknod /dev/chardev c %d 0'.\n", Major);
    printk("<1>Try various minor numbers.  Try to cat and echo to\n");
    printk("the device file.\n");
    printk("<1>Remove the device file and module when done.\n");

    return 0;
}

void cleanup_module(void)
{
    unregister_chrdev(Major, DEVICE_NAME);
}

static int device_open(struct inode *inode, struct file *file)
{
    static int counter = 0;
    if (Device_Open)
        return -EBUSY;
    Device_Open++;
    sprintf(msg, "I already told you %d times Hello world!\n", counter);
    printk(KERN_INFO "mydev: I already told you %d times Hello world!\n", counter++);
    printk(KERN_INFO "mydev: open file %s\n",file->f_path.dentry->d_name.name);
    msg_Ptr = msg;
    try_module_get(THIS_MODULE);
    
    //пытаемся прочитать файл
    /*mm_segment_t old_fs;
	int pruv;
	char buf[100];
		
	old_fs=get_fs();
	set_fs(get_ds());
	pruv = file->f_op->read(file, buf, 100, &file->f_pos);
	set_fs(old_fs);
	printk(KERN_INFO "mydev: in f_open read %d bytes\n", pruv);
	if( pruv > 0)
	{
		printk(KERN_INFO "mydev: reading file %s", buf);
	}*/
    return SUCCESS;
}

static int device_release(struct inode *inode, struct file *file)
{
    Device_Open--;                /* Теперь мы готовы обслужить другой процесс */
    module_put(THIS_MODULE);
    printk(KERN_INFO "mydev: close file\n");
    return 0;
}

static ssize_t device_read(struct file *filp, /* см. include/linux/fs.h   */
                           char *buffer,                        /* буфер, куда надо положить данные */
                           size_t length,                       /* размер буфера */
                           loff_t * offset)
{
	
    int bytes_read = 0;
    if (*msg_Ptr == 0)
        return 0;
	//вывод сообщения в консоль пользователя
    while (length && *msg_Ptr) {

        put_user(*(msg_Ptr++), buffer++);
        length--;
        bytes_read++;
    }
    //sprintf(msg, "read %d bytes\n", bytes_read);
    printk(KERN_INFO "mydev: reading %d bytes\n", bytes_read);
    
    
    /*msg_Ptr = msg;
    try_module_get(THIS_MODULE);*/
    return bytes_read;
}

static ssize_t device_write(struct file *filp, const char *buff, size_t len, loff_t * off)
{
	int bytes_write = 0;
	
	while (len && *buff) {

        get_user(msg_get[bytes_write], buff++);
        len--;
        bytes_write++;
    }
    
	//sprintf(msg, "Sorry, this operation isn't supported.\n");
	/*msg_Ptr = msg;
    try_module_get(THIS_MODULE);*/
    printk(KERN_INFO "mydev: writing %d bytes, write s = %s", bytes_write, msg_get);
    return bytes_write;
}
