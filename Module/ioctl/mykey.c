#include <linux/module.h>
#include <linux/configfs.h>
//#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/tty.h>          /* For fg_console, MAX_NR_CONSOLES */
#include <linux/kd.h>           /* For KDSETLED */
#include <linux/vt.h>
#include <linux/console_struct.h>       /* For vc_cons */
#include <linux/vt_kern.h>

MODULE_DESCRIPTION("Example module illustrating the use of Keyboard LEDs.");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Kondratev");

struct timer_list my_timer;
struct tty_driver *my_driver;
struct file *of;
char kbledstatus = 0;
int fff;
char buf[100]={0,};
char buff;
#define BLINK_DELAY   HZ/5
#define ALL_LEDS_ON   7
#define RESTORE_LEDS  0xFF

void my_timer_callback( unsigned long data )
{
  printk( "my_timer_callback called (%ld).\n", jiffies );
}

static int myreader(void)
{
	mm_segment_t old_fs;
	int pruv;
	
		
		old_fs=get_fs();
		set_fs(get_ds());
		pruv = of->f_op->read(of, buf, 100, &of->f_pos);
		set_fs(old_fs);
	
		
		
		
		
		return pruv;
}

static void my_timer_func(unsigned long ptr)
{
	int r;
	r = myreader();
        if (r == 2)
		{
			printk(KERN_INFO "kbleads: read file mod pruv = %d sys = %s", buf);
			buff = buf[0];
			fff = buff - '0';
			switch(fff)
		{
			case 4: fff = 2;
			break;
			case 2: fff = 4;
			break;
			case 5: fff = 3;
			break;
			case 3: fff = 5;
			/*break;
			default : fff = 7;
			break;*/
		}
		}
		
		
		
        int *pstatus = (int *)ptr;
        if (*pstatus == fff)
                *pstatus = RESTORE_LEDS;
        else
			//*pstatus = fff;
        {
			*pstatus = RESTORE_LEDS;
			(my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
                            *pstatus);
                *pstatus = fff;
			}
        (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
                            *pstatus);
        my_timer.expires = jiffies + BLINK_DELAY;
        add_timer(&my_timer);
}
static int __init kbleds_init(void)
{
	//myreader();
	of = NULL;
        of = filp_open("/proc/hello_proc", O_RDWR, 0);
        if (of == NULL)
        {
			return -1;
		}
		
	
        int i;
        printk(KERN_INFO "kbleds: loading buff = %c\n", buff);
        printk(KERN_INFO "kbleds: fgconsole is %x\n", fg_console);
        
		//pruv = atoi(buf);
        //printf("fff %d\n", MAX_NR_CONSOLES);
        for (i = 0; i < MAX_NR_CONSOLES; i++) {
                if (!vc_cons[i].d)
                        break;
                printk(KERN_INFO "poet_atkm: console[%i/%i] #%i, tty %lx\n", i,
                       MAX_NR_CONSOLES, vc_cons[i].d->vc_num,
                       (unsigned long)vc_cons[i].d->port.tty);
        }
        printk(KERN_INFO "kbleds: finished scanning consoles\n");
        my_driver = vc_cons[fg_console].d->port.tty->driver;
        printk(KERN_INFO "kbleds: tty driver magic %x\n", my_driver->magic);
      
		//timer_create(&my_timer, my_timer_callback, 0 );
		//timer_setup( &my_timer, my_timer_callback, 0 );
		//setup_timer( &my_timer, my_timer_callback, 0 );
        //__init_timer( &my_timer, my_timer_callback, 0 );
        //init_timer_key( &my_timer, 0, NULL, 0 );
        
        init_timer(&my_timer);
        my_timer.function = my_timer_func;
        my_timer.data = (unsigned long)&kbledstatus;
        my_timer.expires = jiffies + BLINK_DELAY;
        add_timer(&my_timer);
        return 0;
}
static void __exit kbleds_cleanup(void)
{
	filp_close(of, NULL);
        printk(KERN_INFO "kbleds: unloading...\n");
        del_timer(&my_timer);
        (my_driver->ops->ioctl) (vc_cons[fg_console].d->port.tty, KDSETLED,
                            RESTORE_LEDS);
}
module_init(kbleds_init);
module_exit(kbleds_cleanup);
