#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>

#define COUNTBEE 2
#define BEEWORK 2
#define BEAREAT 4
#define STARTHONEY 10

union semun {
    int val;                  /* значение для SETVAL */
    struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
    unsigned short *array;    /* массивы для GETALL, SETALL */
    /* часть, особенная для Linux: */
    struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int main( int argc, char *argv[])
{
    char s1[10], s2[10], s3[10];
    int honey = STARTHONEY, i, status; // stat, status;
    pid_t pid[COUNTBEE], wpid;
    key_t key = 69;
    union semun arg;
    int *shm;
    int semid, shmid;
    struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
    struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

    if ((key = ftok(".", 'S')) < 0) {
        printf("Невозможно получить ключ\n");
        exit(1);
    }
    /* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
    semid = semget(key, 1, 0666 | IPC_CREAT);

    /* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);


    /* Создадим область разделяемой памяти */
    if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((shm = (int*)shmat(shmid, NULL, 0)) == -1) {
        printf("shmat");
        exit(1);
    }
    *(shm) = 0;



    sprintf(s2, "%d", semid); //задаем входные для пчел
    sprintf(s1, "%d", shmid);
    sprintf(s3, "%d", BEEWORK);
    printf("%s\n%s\n%s\n", s1, s2, s3);

    for (i = 0; i < COUNTBEE; i++)
    {
        pid[i] = fork();
        if (pid[i] == -1)
        {
            printf("error fork");
            exit(-1);
        } else if (pid[i] == 0)
        {
            if(execl("bee", s1, s2, s3, NULL) < 0)
            {
                printf("error bee\n");
                exit(-2);
            }
        }
    }
    i = 5;
    printf("was %d honey\n", honey);
    //
    while ( honey >= BEAREAT) //жизнь медведя
    {
        sleep(BEAREAT); //ест
        honey -= BEAREAT;
        printf("honey after eating %d\n",honey);
        sleep(BEAREAT); //спит

        //тырим мед у пчел

        /* Получим доступ к разделяемой памяти */
        /*if ((shm = (double*)shmat(shmid, NULL, 0)) == (double *) -1) {
            printf("shmat");
            exit(1);
        }*/
        //printf("доступ к памяти медведем\n");

        /* Заблокируем разделяемую память */
        if((semop(semid, &lock_res, 1)) == -1) {
            printf("Lock failed\n");
            exit(1);
        }
        /*else {
            printf("Semaphore resources decremented by parent (locked)\n");
            fflush(stdout);
        }*/

        honey += *(shm);
        *(shm) = 0;
        printf("honey after gather %d\n",honey);

        /* Освободим разделяемую память */
        if((semop(semid, &rel_res, 1)) == -1) {
            printf("Unlock failed\n");
            exit(1);
        }
        /*else {
            printf("Semaphore resources incremented by one (unlocked) i=%d\n", i);
            fflush(stdout);
        }*/
    }

    /* Заблокируем разделяемую память */
    if((semop(semid, &lock_res, 1)) == -1) {
        printf("Lock failed\n");
        exit(1);
    } else {
        printf("Semaphore resources decremented by parent (locked)\n");
        fflush(stdout);
    }
    *(shm) = -1; //для смерти пчел
    /* Освободим разделяемую память */
    if((semop(semid, &rel_res, 1)) == -1) {
        printf("Unlock failed\n");
        exit(1);
    } else {
        printf("Semaphore resources incremented by one (unlocked) i=%d\n", i);
        fflush(stdout);
    }
    for (int i = 0; i < COUNTBEE; i++) {
        wpid = waitpid(pid[i], &status, 0);
        if (pid[i] == wpid) {
            printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(status));
            fflush(stdout);
        }
    }

    if (shmdt(shm) < 0) {
        printf("Ошибка отключения\n");
        exit(1);
    }

    /* Удалим созданные объекты IPC */
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        printf("Невозможно удалить область\n");
        exit(1);
    } else
        printf("Сегмент памяти помечен для удаления\n");

    if (semctl(semid, 0, IPC_RMID) < 0) {
        printf("Невозможно удалить семафор\n");
        exit(1);
    }

    return 0;
}
