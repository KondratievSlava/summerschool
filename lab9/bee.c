#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>

//#include <stdlib.h>
//#include <stdio.h>
//#include "myqueue.h"

union semun {
    int val;                  /* значение для SETVAL */
    struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
    unsigned short *array;    /* массивы для GETALL, SETALL */
    /* часть, особенная для Linux: */
    struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int main (int argc, char *argv[])
{
    int shmid;
    int *shm;
    int i, honey;

    int semid;
    //union semun arg;
    struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
    struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

    srand(getpid());
    if( argc < 3 )
    {
        printf("not find arguments\n");
        return -1;
    }
    printf("bee started\n");
    int beeHoney;
    //long qtype;

    beeHoney = atoi(argv[2]);
    semid = atoi(argv[1]);
    shmid = atoi(argv[0]);
    //msgqid = atoi(argv[0]);
    
    /* Получим доступ к разделяемой памяти */
    if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
        printf("shmat");
        exit(1);
    }
    //printf("получение доступа к памяти пчелой\n");

    /* Заблокируем разделяемую память */
    if((semop(semid, &lock_res, 1)) == -1) {
        printf("Lock failed\n");
        exit(1);
    } /*else {
        printf("Semaphore resources decremented by bee (locked) \n");
        fflush(stdout);
    }
    printf("блокирование памяти пчелой\n");*/
    
    honey = *(shm);
   
    /* Освободим разделяемую память */
    if((semop(semid, &rel_res, 1)) == -1) {
        printf("Unlock failed\n");
        exit(1);
    } /*else {
        printf("Semaphore resources incremented by bee (unlocked)\n");
        fflush(stdout);
    }*/
    while( honey != -1 )
    {
		i = rand() % 16 + 4;
        printf("sleep %d sec\n",i);
        sleep(i);
        /* Заблокируем разделяемую память */
        if((semop(semid, &lock_res, 1)) == -1) {
            printf("Lock failed\n");
            exit(1);
        } /*else {
            printf("Semaphore resources decremented by bee (locked) pid=%d\n", getpid());
            fflush(stdout);
        }*/
		
		printf("honey prev bee %d\n", *(shm));
        honey = *(shm);
        
        if(honey > -1)
			*(shm) = *(shm) + beeHoney;
        
        printf("honey after bee %d\n", *(shm));
	
        /* Освободим разделяемую память */
        if((semop(semid, &rel_res, 1)) == -1) {
            printf("Unlock failed\n");
            exit(1);
        }/* else {
            printf("Semaphore resources incremented by bee (unlocked) pid=%d\n", getpid());
            fflush(stdout);
        }*/
    }

    /* Отключимся от разделяемой памяти */
    if (shmdt(shm) < 0) {
        printf("Ошибка отключения\n");
        exit(1);
    }
    printf("смерть пчелы\n");
    exit(0);
	
    //scanf("%d", &qtype);
    //exit(5);
}

