#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int StepMas=4;
const int LengthWord=255;

struct elem
{
	char name[255];
	int year;
	char department[255];
	int salary;
};
typedef struct elem typeStruct;

struct vect
{
	typeStruct *mas;
	int num;
	int maxNum;
};
typedef struct vect typeStructMas;

typeStructMas* initBase( typeStructMas *base)
{
	base=malloc(sizeof(typeStructMas)); 
	
	base->mas=malloc(StepMas * sizeof(typeStruct) );
	base->num=0;
	base->maxNum=StepMas;
	return base;
}

typeStructMas* inputBase (typeStructMas *base)
{
	FILE *in;
	int year, sal;
	char name[255], dep[255];
	in = fopen("base.txt","r");
	if (in == NULL)
	{
		printf("file not found");
		exit ;
	}
	fscanf(in,"%s %d %s %d", name, &year, dep, &sal);
	/*fscanf(in,"%s %d %s %d",
	 base->mas[base->num].name, &base->mas[base->num].year,
	 base->mas[base->num].department,
	  &sal);*/
	while ( feof(in) == 0 )
	{
		if (base->num >= base->maxNum )
		{
			base->mas = realloc(base->mas, (base->maxNum + StepMas) * sizeof(typeStruct) );
			base->maxNum += StepMas;
		}
		
		
		strcpy( base->mas[base->num].name, name);
		base->mas[base->num].year=year;
		strcpy( base->mas[base->num].department, dep);
		base->mas[base->num].salary=sal;
		
		base->num++;
		
		fscanf(in,"%s %d %s %d", name, &year, dep, &sal);
	}
	fclose(in);
	return base;
}

void outputBase( typeStructMas *base)
{
	int i;
	for (i=0; i<base->num; i++)
	{
		printf("сотрудник № %d:\nИмя: %s\nДата рождения: %d\nОтдел №%s\nОклад: %d\n\n",
		i+1,
		base->mas[i].name, base->mas[i].year,
		base->mas[i].department, base->mas[i].salary);
	}
}

int compare1(const void *x1, const void *x2)
{
	typeStruct a = *(typeStruct*) x1;
	typeStruct b = *(typeStruct*) x2;
	return (a.year - b.year);
}

int main()
{
	typeStructMas *base=NULL;
	base = initBase(base);
	//ввод данных
	base = inputBase(base);
	//сортировка
	qsort(base->mas,base->num,sizeof(typeStruct),compare1);
	//вывод базы
	outputBase(base);
	return 0;
}
