#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define Word  80
#define FileOut "temp.txt"

int main(int argc, char* argv[])
{
	int n,pruv;
	char  *ss;
	ss=calloc(sizeof(char),Word);
	FILE *in, *out;
	if (argc != 3 )
	{ 
		printf("неверное количество параметров");
		return -1;
	}
	pruv = atoi(argv[2]);
	in = fopen (argv[1],"r");
	if ( in == NULL)
	{
		printf("нет исходного файла");
		return 0;
	}
	out = fopen (FileOut, "w");
	if ( out == NULL)
	{
		printf("нет выходного файла");
		return 0;
	}
	fgets(ss,Word,in);
	while ( !feof(in) )
	{
		
		n = strlen(ss)-1;
		if (n <= pruv )
			fputs(ss, out);
		fgets(ss,Word,in);
	}
	fclose(in);
	fclose(out);
	free(ss);
	remove(argv[1]);
	rename(FileOut, argv[1]);
	
    return 0;
 }

