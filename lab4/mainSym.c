#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define Word  80
#define FileOut "temp.txt"

int main(int argc, char* argv[])
{
	
	char c,pruv;
	FILE *in, *out;
	if (argc != 3 )
	{ 
		printf("неверное количество параметров");
		return -1;
	}
	
	in = fopen (argv[1],"r");
	if ( in == NULL)
	{
		printf("нет исходного файла");
		return 0;
	}
	out = fopen (FileOut, "w");
	if ( out == NULL)
	{
		printf("нет выходного файла");
		return 0;
	}
	pruv = argv[2][0];
	//printf("symbol %c\n",pruv);
	
	fscanf(in,"%c",&c);
	while ( !feof(in) )
	{
		if ( c != pruv )
			fprintf(out,"%c",c);
		fscanf(in,"%c",&c);
	}
	fclose(in);
	fclose(out);
	
	remove(argv[1]);
	rename(FileOut, argv[1]);
	
    return 0;
 }

