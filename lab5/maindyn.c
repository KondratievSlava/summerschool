#include <stdio.h>
#include <dlfcn.h>

#define DYNLIB "libdynlib.so"

int main()
{
	void *lib_hand;
	lib_hand = dlopen(DYNLIB,RTLD_LAZY);
	if (!lib_hand)
	{
		printf("нет динамической библиотеки");
		return -1;
	}
	int (*sumfunc)(int a, int b);
	int (*diffunc)(int a, int b);
	sumfunc = dlsym(lib_hand, "mysum");
	diffunc = dlsym(lib_hand, "mydif");
	
	int s1,s2,res=0;
    printf("введите два числа для сложения\n");
    scanf("%d%d",&s1,&s2);
    res = sumfunc(s1,s2);
    printf("сумма равна %d\n",res);
    printf("введите два числа для разности\n");
    scanf("%d%d",&s1,&s2);
    res = diffunc(s1,s2);
    printf("разность равна %d\n",res);
    
    dlclose(lib_hand);
	
}
